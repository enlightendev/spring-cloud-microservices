# About

Service discovery 

http://cloud.spring.io/spring-cloud-netflix/spring-cloud-netflix.html


# Running 

Eureka needs to be the first service that is up and running since all
other services will register with eureka.

# Docker

- build image: docker build -t microservicedemo/eureka .
- run container: docker run -P -p 9998:9998 -d --name eureka microservicedemo/eureka

exposing its ports (-P)
map container port to host port (-p 9998:9998)
in the background (-d)
naming it eureka (–name)

- docker images

