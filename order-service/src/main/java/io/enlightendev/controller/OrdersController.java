package io.enlightendev.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 */
@RestController
@RequestMapping("/api/orders")
public class OrdersController {

	@RequestMapping("/all")
	public String getOrders(){
		return "All orders :-)";
	}
}
