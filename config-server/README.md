# About

config server plays a role in 12 factor apps in that it provides
a centralized and external repository for configuration info.

applications, apis, or services upon startup will look to this service
to get runtime attributes.

# Spring 

@EnableConfigServer - 
@EnableDiscoveryClient - 

# Docker

- build image: 
docker build -t microservicedemo/config .

- run container: 
NOTE: must mount local git repo - https://docs.docker.com/engine/tutorials/dockervolumes/

- docker run -v /Users/Juan/dev/micro-services/spring/spring-cloud-microservices/git-config:/git-config --link eureka -P -p 9999:9999 -d --name config microservicedemo/config
