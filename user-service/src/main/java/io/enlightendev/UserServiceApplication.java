package io.enlightendev;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.web.bind.annotation.RequestMapping;

import io.enlightendev.domain.User;
import io.enlightendev.repository.UserRepository;

@SpringBootApplication
@EnableEurekaClient
public class UserServiceApplication implements CommandLineRunner {

	private static final Logger log = LoggerFactory.getLogger(UserServiceApplication.class);

	@Autowired
	private UserRepository repository;

	@RequestMapping("/")
	public String home() {
		return "Hello World";
	}

	public static void main(String[] args) {
		SpringApplication.run(UserServiceApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {

		// save a couple of customers
		repository.save(new User("Jack", "Bauer"));
		repository.save(new User("Chloe", "O'Brian"));
		repository.save(new User("Kim", "Bauer"));
		repository.save(new User("David", "Palmer"));
		repository.save(new User("Michelle", "Dessler"));

		for (User user : repository.findAll()) {
			log.info(user.toString());
		}
	}
}