package io.enlightendev.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import io.enlightendev.domain.User;
import io.enlightendev.repository.UserRepository;

/**
 *
 */
@RestController
@RequestMapping("/api/users")
public class UserController {

    @Autowired
    UserRepository userRepository;

    @RequestMapping("/search/{lastname}")
    public @ResponseBody List<User> getUsers(@PathVariable("lastname") String lastname) {

        return userRepository.findByLastName(lastname);

    }


}